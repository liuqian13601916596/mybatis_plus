package com.ljs.mybatis_plus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.ljs.mybatis_plus.enums.AgeEnum;

import com.ljs.mybatis_plus.generator.entity.Student;
import com.ljs.mybatis_plus.generator.mapper.StudentMapper;
import com.ljs.mybatis_plus.vo.ClassesVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class MybatisPlusApplicationTests {
    @Resource
    StudentMapper studentMapper;

    @Test
    void selectMybatisplus() {
        studentMapper.selectList(null).forEach(System.out::print);//不根据条件查询

    }

    @Test
    void testId() {
        Student student = new Student();
        student.setName("雪花自增");
        student.setPassword("123456");
        studentMapper.insert(student);//存入数据库
    }

    @Test
    void testTime() {

        Student student = new Student();
        student.setName("新增age状态");
        student.setPassword("123456");


        studentMapper.insert(student);//存入数据库

    }
    @Test
    void updateTime(){
        //先找到对应的数据，来修改
          Student student=studentMapper.selectById(64);
        student.setName("修改时间和version");
        studentMapper.updateById(student);
        Student student1=studentMapper.selectById(64);
        student.setName("修改时间和version1");
        studentMapper.updateById(student1);
    }
    //测试删除
    @Test
    void delete(){

       // studentMapper.deleteById(67);逻辑删除
        studentMapper.deleteBatchIds(Arrays.asList(3,4));//如果不用逻辑删除，就把deleted的数据库的字段删掉，现在删除数据库的字段deleted变为了1就表示删除成功
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("id",8);
        studentMapper.delete(queryWrapper);//根据条件来删除，
    }
    @Test
    //按条件查询
    void selectbyWhere(){
        QueryWrapper queryWrapper=new QueryWrapper();
        QueryWrapper queryWrapper1=new QueryWrapper();
        QueryWrapper queryWrapper3=new QueryWrapper();
        QueryWrapper queryWrapper4=new QueryWrapper();
        QueryWrapper queryWrapper5=new QueryWrapper();
        queryWrapper.eq("name","狗娃子");//按照name的条件来查询
      List list=studentMapper.selectList(queryWrapper)  ;
        System.out.println("狗娃子:"+studentMapper.selectList(queryWrapper));
        Map<String,Object> map=new HashMap<>();
        map.put("name","李四");
        map.put("age",3);
        queryWrapper1.allEq(map);//多条件查询
        System.out.println(" 有同名的通过其他条件来查 "+studentMapper.selectList(queryWrapper1));
        queryWrapper3.gt("age",2);//年龄大于2的，
//        queryWrapper3.ge("age",2);//年龄大于等于2的，
//        queryWrapper3.lt("age",2);//年龄小于2的，
//        queryWrapper3.le("age",2);//年龄小于等于2的，

        System.out.println(" 年龄大于2的 "+studentMapper.selectList(queryWrapper3));
        //queryWrapper4.likeRight("name","小");//右模糊查询  小%，百分号在右边
        queryWrapper4.like("name","张");//模糊查询，两个百分号
        //queryWrapper4.likeLeft("name","小");//左模糊查询  %小，百分号在左边

        System.out.println("模糊查询"+studentMapper.selectList(queryWrapper4));
        queryWrapper5.inSql("id","select id from student where id>3");//联合查询，以id和age作为条件
        queryWrapper5.inSql("age","select age from student where age<3");
        System.out.println("age条件"+studentMapper.selectList(queryWrapper5));
        queryWrapper5.orderByAsc("age");
//        queryWrapper5.having("id<15");
        System.out.println("age条件根据age排序"+studentMapper.selectList(queryWrapper5));
        //查询个数，就相当于是select count（*） from student;
      int count=  studentMapper.selectCount(queryWrapper5);
        System.out.println("数量"+count);

        studentMapper.selectBatchIds(Arrays.asList(1,2,3)).forEach(System.out::println);
        Page<Student> page=new Page(2,4);

        Page<Student> page1 =studentMapper.selectPage(page,null);
        //总共的数量
          Long total= page1.getTotal();
        System.out.println("总共的数量"+total);
        page1.getSize();
        System.out.println("总共的size"+page1.getSize());
        List<Student> goodsList = studentMapper.selectPage(page, null).getRecords();

       System.out.println("分页的个数"+goodsList.size());
        goodsList.forEach(System.out::print);

//        page1.getRecords().forEach(System.out::println);
    }
    @Test
    //多表查询
    void ConnectionSel(){
      List<ClassesVo> list= studentMapper.studentList("一班");
        System.out.println("多表查询二班的学生情况"+list);

    }
    @Test
    void update(){
        Student student=studentMapper.selectById(9);
        student.setName("小久");
        studentMapper.updateById(student);
    }



}
