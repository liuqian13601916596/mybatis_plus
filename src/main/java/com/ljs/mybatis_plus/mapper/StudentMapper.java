package com.ljs.mybatis_plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljs.mybatis_plus.entity.NewStudent;
import com.ljs.mybatis_plus.generator.entity.Student;
import com.ljs.mybatis_plus.vo.ClassesVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public  interface  StudentMapper  extends BaseMapper<Student> {
   // @Select("select c.* ,s.name sname from student s ,classes c where s.id=c.id and c.cname=#{cname} ")
    List<ClassesVo> studentList (String cname);//根据班级来查
}
