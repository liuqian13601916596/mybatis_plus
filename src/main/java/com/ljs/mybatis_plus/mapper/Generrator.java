package com.ljs.mybatis_plus.mapper;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class Generrator {
    public static void main(String[] args) {
        AutoGenerator autoGenerator=new AutoGenerator();
        //数据源
        DataSourceConfig dataSourceConfig=new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setUrl("jdbc:mysql://localhost:3306/mybatis?serverTimezone=UTC");
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("ljs123456");
        //将数据源加入自动生成器
        autoGenerator.setDataSource(dataSourceConfig);
        //全局配置
        GlobalConfig globalConfig=new GlobalConfig();
        globalConfig.setOutputDir(System.getProperty("user.dir")+"/src/main/java");//生成到java的路径
        globalConfig.setAuthor("ljs");//设置作者
        globalConfig.setOpen(false);
        globalConfig.setServiceName("%sService");//这个设置接口名不会带I
        //将全局配置加入到自动生成器
        autoGenerator.setGlobalConfig(globalConfig);
        //包信息
        PackageConfig packageConfig=new PackageConfig();
        packageConfig.setParent("com.ljs.mybatis_plus");
        packageConfig.setModuleName("generator");//模板名
        packageConfig.setEntity("entity");
        packageConfig.setController("contorller");
        packageConfig.setMapper("mapper");
        packageConfig.setService("service");
        packageConfig.setServiceImpl("service.impl");
//将包信息添加到生成器
        autoGenerator.setPackageInfo(packageConfig);
        //配置策略
        //加上lombok
        StrategyConfig strategyConfig=new StrategyConfig();
        strategyConfig.setEntityLombokModel(true);//加lombok
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);//驼峰命名法。
        strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel);
        autoGenerator.setStrategy(strategyConfig);
        //最后还要执行
        autoGenerator.execute();
    }
}
