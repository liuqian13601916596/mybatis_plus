package com.ljs.mybatis_plus.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 用来处理创建时间更新时间的操作
 */
@Component//记得加Component加入到sringioc,不然没有用
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setInsertFieldValByName("create_time",new Date(),metaObject);
        this.setUpdateFieldValByName("update_time",new Date(),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
          this.setUpdateFieldValByName("update_time",new Date(),metaObject);
    }
}
