package com.ljs.mybatis_plus.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

public enum  StateEnum {
    outclass(0,"下课"),//是逗号
    inclass(1,"上课");

    StateEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    @EnumValue
    private  Integer code;
    private  String  msg;


}
