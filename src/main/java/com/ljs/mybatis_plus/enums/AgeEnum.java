package com.ljs.mybatis_plus.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum AgeEnum implements IEnum<Integer> {
    ONE(1,"1岁"),
    TOW(2,"两岁"),
    Three(3,"两岁")
    ;


    @Override
    public Integer getValue() {
        return this.code;//返回code
    }
    private Integer code;
    private  String msg;

    AgeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
