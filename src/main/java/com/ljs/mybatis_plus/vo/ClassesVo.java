package com.ljs.mybatis_plus.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用来接收多表关联，命名要和数据库名一样，不一样的话，要在mapper接口中的列名取别名
 */
@Data
public class ClassesVo {
    private int cid;
    private  String cname;
    private  int id;
    private String sname;//这个和数据库名不一样
}
