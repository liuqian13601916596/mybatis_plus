package com.ljs.mybatis_plus.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.ljs.mybatis_plus.enums.AgeEnum;
import com.ljs.mybatis_plus.enums.StateEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "student")//指定表名
public class NewStudent implements Serializable {
    @TableId(type=IdType.AUTO)//默认不写的话是type = IdType.NONE，雪花算法生成随机数，要用Long类型,同时数据库的大小也要匹配
//    @TableId(type=IdType.INPUT)要自己手动去赋值
    private  Long id;
    @TableField(value = "name" ,select = true)//如果实体类的定义和数据库字段不一样，映射到数据库的字段，用于映射非主键字段,select是是否查，默认不写是查的
    private String name;
    private  String password;

    private AgeEnum age;//年龄的枚举
    private String pic;
    @TableField(exist = false)//下面这个是自己定义，数据库不存在的字段，就用exist=false,就不会去查，不会报错了
    private  String haha;
    /**
     * @create_time创建的时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;
    /**
     * @create_time修改的时间
     */
    @TableField(fill =FieldFill.INSERT_UPDATE)//要创建自动填充时间的处理器
    private Date update_time;

    @Version
    private  Integer version;//乐观锁要去配置个乐观锁拦截器，数据库的version默认为1
    private StateEnum state;
    @TableLogic
    private  Integer deleted;
}
