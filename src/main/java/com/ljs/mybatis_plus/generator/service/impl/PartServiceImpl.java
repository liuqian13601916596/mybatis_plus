package com.ljs.mybatis_plus.generator.service.impl;

import com.ljs.mybatis_plus.generator.entity.Part;
import com.ljs.mybatis_plus.generator.mapper.PartMapper;
import com.ljs.mybatis_plus.generator.service.PartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Service
public class PartServiceImpl extends ServiceImpl<PartMapper, Part> implements PartService {

}
