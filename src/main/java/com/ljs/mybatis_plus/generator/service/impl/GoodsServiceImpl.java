package com.ljs.mybatis_plus.generator.service.impl;

import com.ljs.mybatis_plus.generator.entity.Goods;
import com.ljs.mybatis_plus.generator.mapper.GoodsMapper;
import com.ljs.mybatis_plus.generator.service.GoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {

}
