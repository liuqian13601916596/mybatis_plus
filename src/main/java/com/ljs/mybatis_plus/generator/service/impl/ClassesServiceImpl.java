package com.ljs.mybatis_plus.generator.service.impl;

import com.ljs.mybatis_plus.generator.entity.Classes;
import com.ljs.mybatis_plus.generator.mapper.ClassesMapper;
import com.ljs.mybatis_plus.generator.service.ClassesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Service
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, Classes> implements ClassesService {

}
