package com.ljs.mybatis_plus.generator.service.impl;

import com.ljs.mybatis_plus.generator.entity.Orders;
import com.ljs.mybatis_plus.generator.mapper.OrdersMapper;
import com.ljs.mybatis_plus.generator.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

}
