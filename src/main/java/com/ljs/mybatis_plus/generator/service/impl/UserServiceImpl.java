package com.ljs.mybatis_plus.generator.service.impl;

import com.ljs.mybatis_plus.generator.entity.User;
import com.ljs.mybatis_plus.generator.mapper.UserMapper;
import com.ljs.mybatis_plus.generator.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
