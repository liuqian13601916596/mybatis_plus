package com.ljs.mybatis_plus.generator.service.impl;

import com.ljs.mybatis_plus.generator.entity.Customer2;
import com.ljs.mybatis_plus.generator.mapper.Customer2Mapper;
import com.ljs.mybatis_plus.generator.service.Customer2Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Service
public class Customer2ServiceImpl extends ServiceImpl<Customer2Mapper, Customer2> implements Customer2Service {

}
