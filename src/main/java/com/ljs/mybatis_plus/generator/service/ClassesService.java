package com.ljs.mybatis_plus.generator.service;

import com.ljs.mybatis_plus.generator.entity.Classes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
public interface ClassesService extends IService<Classes> {

}
