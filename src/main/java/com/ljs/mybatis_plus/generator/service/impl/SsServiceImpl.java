package com.ljs.mybatis_plus.generator.service.impl;

import com.ljs.mybatis_plus.generator.entity.Ss;
import com.ljs.mybatis_plus.generator.mapper.SsMapper;
import com.ljs.mybatis_plus.generator.service.SsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * VIEW 服务实现类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Service
public class SsServiceImpl extends ServiceImpl<SsMapper, Ss> implements SsService {

}
