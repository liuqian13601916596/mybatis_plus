package com.ljs.mybatis_plus.generator.service;

import com.ljs.mybatis_plus.generator.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
public interface StudentService extends IService<Student> {

}
