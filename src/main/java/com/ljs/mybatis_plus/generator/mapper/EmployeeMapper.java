package com.ljs.mybatis_plus.generator.mapper;

import com.ljs.mybatis_plus.generator.entity.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}
