package com.ljs.mybatis_plus.generator.mapper;

import com.ljs.mybatis_plus.generator.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljs.mybatis_plus.vo.ClassesVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
public interface StudentMapper extends BaseMapper<Student> {
    @Select("select c.* ,s.name sname from student s ,classes c where s.id=c.id and c.cname=#{cname} ")
    List<ClassesVo> studentList (String cname);//根据班级来查
}
