package com.ljs.mybatis_plus.generator.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
public class Employee implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "eid", type = IdType.AUTO)
      private Integer eid;

    private String ename;

    private Integer age;


}
