package com.ljs.mybatis_plus.generator.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
public class Customer2 implements Serializable {

    private static final long serialVersionUID=1L;

      private Integer id;

    private String name;

    private Integer money;


}
