package com.ljs.mybatis_plus.generator.contorller;


import com.ljs.mybatis_plus.generator.entity.Student;
import com.ljs.mybatis_plus.generator.service.StudentService;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ljs
 * @since 2020-08-20
 */
@Controller
@RequestMapping("/generator/student")
public class StudentController {
    @Resource
    StudentService studentService;
    @RequestMapping("/test")
    public ModelAndView test(){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("index");
      List<Student> studentList=studentService.list();
        modelAndView.addObject("studentList",studentList);
        return modelAndView;
    }
    @RequestMapping("/index")
    public String toindex(){
        return "index";
    }

}

