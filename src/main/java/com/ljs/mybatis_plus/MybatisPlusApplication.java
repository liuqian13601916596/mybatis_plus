package com.ljs.mybatis_plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//@MapperScan(value = {"com.ljs.mybatis_plus.generator.mapper","com.ljs.mybatis_plus.mapper"})//扫描接口.自动生成后，要修改一下mapper的包,因为自己写的和自动生成的实体类一样，所以得删除一个
@MapperScan("com.ljs.mybatis_plus.generator.mapper")
@SpringBootApplication
public class MybatisPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusApplication.class, args);
    }

}
